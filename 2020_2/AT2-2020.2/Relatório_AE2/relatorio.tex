% ------------------------------------------------------------%
% 2020-12-05
% 2015-2020 - Emerson Ribeiro de Mello - mello@ifsc.edu.br
% ------------------------------------------------------------%
\documentclass[11pt]{classes/ifscarticle}
\usepackage{classes/ifscutils}
\usepackage[alf]{abntex2cite} % Citações padrão ABNT

\renewcommand*{\theusecase}{UC-\thesection.\arabic{usecase}}


\usepackage{lipsum}

\AtBeginDocument{\thispagestyle{empty}}
\begin{document}
% ------------------------------------------------------------%
% Capa
% ------------------------------------------------------------%
\begin{center}

% Logotipo do órgão
\includegraphics[scale=.7]{classes/imagens/ifsc-v}
\vspace{7.5cm}

% Título
{\huge \bfseries Relatório Sistemas de Comunicação 1}

\vspace{.5cm}

% Subtítulo
{\large \bfseries  Laboratório - Modulação em Amplitude}

\vfill
\end{center}

% Autor(es)
{\noindent \large \bfseries 
Sarom Torres%
%\\[.5em] Segundo Autor do Trabalho%
}

% Data que o relatório foi gerado
\begin{flushright}
23 de dezembro de 2020
\end{flushright}

\clearpage
\pagestyle{firstpage}
% ------------------------------------------------------------%

% ------------------------------------------------------------%
% Adicionando sumário
% ------------------------------------------------------------%
\tableofcontents
\clearpage

% ------------------------------------------------------------%
% Início do documento
% ------------------------------------------------------------%

\section{Introdução}
\label{cap:introducao}

O presente relatório tem por objetivo detalhar as atividades de laboratório referentes à Modulação em Amplitude da disciplina de Sistemas de Comunicação 1. 
Tais atividades visam a sedimentação dos conceitos de modulação e multiplexação de sinais, tais como: AM-DSB, AM-DSB-SC, FDM, transmissão e recuperação de sinais modulados, entre outros.

Para este fim, foram desenvolvidas duas atividades. A primeira atividade (\autoref{sec:A1}) consiste na realização da modulação AM-DSB 
de um sinal a fim de observar os efeitos da variação do fator de modulação. Além disso, nessa atividade é 
realizada a modulação AM-DSB-SC e a demodulação do sinal.

Na segunda atividade (\autoref{sec:A2}) três sinais com diferentes características foram multiplexados a fim de observar
os processos de modulação e filtragem necessários para a transmissão de diversos sinais em um mesmo canal.

Para as simulações realizadas foi utilizado o software Octave. Na \autoref{sec:conceitos} são apresentados breves resumos sobre
a base teórica necessária no desenvolvimento das atividades e na \autoref{sec:atividades} são descritas as atividades desenvolvidas e debatidos os resultados obtidos. 


\section{Conceitos Teóricos}
\label{sec:conceitos}

Sistemas de comunicação envolvem a transmissão e a recepção de sinais. Os sinais oriundos de 
uma fonte de informação são chamados de sinal em \textbf{banda base} e podem ser transmitidos através de diferentes meios de comunicação. 
Entretanto, a fim de garantir uma maior eficiência na propagação do sinal e 
para utilizar antenas de menor tamanho é realizado o processo de modulação do sinal.

A \textbf{modulação} pode ser realizada alterando as características (amplitude, frequência ou fase) de uma onda portadora 
a fim de representar a informação contida no sinal banda base. 
No que diz respeito aos sinais analógicos os principais tipos de modulação são:

\begin{itemize}
    \item [$\bullet$]Modulação em amplitude:
    \begin{itemize}
        \item [$\bullet$]AM-DSB: Modulação de Banda Lateral Dupla;
        \item [$\bullet$]AM-DSB-SC: Modulação de Banda Lateral Dupla e Portadora Suprimida;
        \item [$\bullet$]AM-SSB: Modulação de Banda Latetal Única;
        \item [$\bullet$]AM-VSB: Modulação de Banda Lateral Vestigial.
    \end{itemize}
    \item [$\bullet$]Modulação em ângulo: 
    \begin{itemize}
        \item [$\bullet$]FM: Modulação em Frequência;
        \item [$\bullet$]PM: Modulação em Fase.
    \end{itemize}
\end{itemize}


Além da modulação, a \textbf{multiplexação} dos sinais pode ser realizada a fim de transmitir diversos sinais em um mesmo meio 
físico sem que ocorra interferência entre seus canais. 
Os principais tipos de multiplexação são:

\begin{itemize}
    \item [$\bullet$]FDM: Multiplexação por divisão do espectro de frequência;
    \item [$\bullet$]TDM: Multiplexação por divisão do tempo;
    \item [$\bullet$]WDM: Multiplexação por divisão do comprimento de onda;
    \item [$\bullet$]CDM:  Multiplexação por divisão de Código.
\end{itemize}

Neste documento serão abordadas as modulações AM-DSB, AM-DSB-SC e a multiplexação FDM.

\subsection{Modulação em amplitude (AM)}
\label{sec:am}

A \textbf{Modulação em Amplitude} (AM) consiste na variação da amplitude da onda portadora conforme o sinal modulante (sinal banda base). 
Dessa maneira, a informação a ser transmitida está contida no envoltório da onda modulada.

Para a realização dessa basta muitiplicar o sinal modulante por um sinal senoidal de 
frequência maior que o sinal banda base. Por meio dessa multiplicação entre os sinais o espectro do sinal modulante 
será deslocado em função da frequência da portadora. 

%------------------------------|
%            AM-DSB            |
%------------------------------|

\subsection{AM-DSB}
\label{sec:am-dsb}

Na Modulação de Banda Lateral Dupla (AM-DSB) o sinal da portadora é enviado junto com o sinal modulado. 
Para este fim uma componente contínua é adicionada ao sinal modulante. A \autoref{eq:am-dsb1} e a \autoref{eq:am-dsb2}
apresentam o modelo matemático da modulação AM-DSB.

 \begin{equation}
     s(t) = [A_0 + m(t)]\times c(t) 
     \label{eq:am-dsb1}
 \end{equation}

 \begin{equation}
    s(t) = [A_0 \times c(t)] + [m(t) \times c(t)] 
    \label{eq:am-dsb2}
\end{equation}

Em que:

\begin{itemize}
    \item A\textsubscript{0}: componente contínua;
    \item m(t): sinal modulante ou sinal banda base;
    \item c(t): onda portadora;
    \item s(t): sinal transmitido.
\end{itemize}

É possível observar que por meio da adição da componente contínua realizada 
na \autoref{eq:am-dsb1} o sinal a ser transmitido será composto por pelo sinal modulado juntamente com a 
portadora.

A adição de uma componente contínua permite com que esse sinal seja facilmente detectado e demodulado no receptor.
Entretanto uma quantia significativa de potência é transmitida na onda portadora o que acarreta
em transmissões mais dispendiosas dos sinais, 
por vezes gastando mais potência para a transmissão da portadora do que para sinal modulado.

Além disso, a informação nesse tipo de modulação está contida nas duas bandas laterais do sinal.


%------------------------------|
%     FATOR DE MODULAÇÃO       |
%------------------------------|
\subsubsection{Fator de Modulação}
\label{sec:fator-am}

O \textbf{fator de modulação} é a relação entre a amplitude (A\textsubscript{m}) do sinal modulante 
e a componente contínua adicionada ao sinal (A\textsubscript{0}). (\autoref{eq:fator})

\begin{equation}
   \mu = \frac{A_m}{A_0} 
    \label{eq:fator}
\end{equation}

Caso essa relação seja menor que 1 não ocorre a inversão de fase, entretanto como o A\textsubscript{0} multiplica
apenas a portadora quanto maior seu valor mais potência é colocada na portadora. 
O estado desejável é que o fator de modulação esteja próximo a 1, dessa forma coloca-se menos potência
na portadora e mais no sinal. O fator ser maior que 1 não é desejável visto que 
ocorre a inversão de fase.


%------------------------------|
%         AM-DSB-SC            |
%------------------------------|
\subsection{AM-DSB-SC}
\label{sec:am-dsb-sc}

Nesse tipo de modulação a onda portadora é suprimida e não é transmitida junto como o 
sinal modulado, conforme \autoref{eq:am-dsb-sc}. 

\begin{equation}
    s(t) = m(t)\times c(t) 
    \label{eq:am-dsb-sc}
\end{equation}

Em que:

\begin{itemize}
    \item m(t): sinal modulante ou sinal banda base;
    \item c(t): onda portadora;
    \item s(t): sinal modulado.
\end{itemize}

A não transmissão da portadora evita que potência seja desperdiçada, contudo ao não adicionar
uma componente contínua ao sinal modulante ocorre a inversão de fase do sinal modulado, exigindo
circuitos mais complexos para a demodulação do sinal.
Nesse método de modulação ambas bandas laterais são transmitidas. 

%------------------------------|
%             FDM              |
%------------------------------|
\subsection{FDM}
\label{sec:fdm}

A multiplexação por divisão de frequência permite com que vários sinais sejam transmitidos em um
mesmo canal. Esse processo é realizado através do deslocamento de cada um dos sinais banda base para
canais de frequência pré determinados antes juntá-los em um único sinal a ser transmitido.


%------------------------------|
%       Atividade 1            |
%------------------------------|

\section{Atividade 1}
\label{sec:A1}

A primeira simulação consiste em realizar um processo de modulação AM DSB e AM DSB-SC. O 
objetivo de realizar a modulação AM-DSB é observar seu espectro e entender o efeito causado ao variar o fator de modulação. Já a 
modulação AM-DSB-SC visa compreender os passos necessários para o processo de demodulação do sinal.

\subsection{Modulação AM-DSB-SC}
A \autoref{eq:m_t} representa matematicamente o sinal modulante de 
1kHz e a \autoref{eq:c_t} descreve e a onda portadora de 10kHz.
Ambos os sinais estão representados na \autoref{fig:at1-am-dsb-sc}.(a)  e \autoref{fig:at1-am-dsb-sc}.(c), bem como
seus espectros de frequência \autoref{fig:at1-am-dsb-sc}.(b) e \autoref{fig:at1-am-dsb-sc}.(d), respectivamente.

 \begin{equation}
    m(t) = 2\times\cos (2\pi\times10^3t)
    \label{eq:m_t}
 \end{equation}

 \begin{equation}
    c(t) = 1\times\cos (20\pi\times10^3t)
     \label{eq:c_t}
 \end{equation}

A modulação foi realizada através da multiplicação descrita na \autoref{eq:am-dsb-sc} e é 
representada na \autoref{fig:at1-am-dsb-sc}.(e). É possível observar que a 
amplitude da onda modulada varia de acordo com o sinal modulante, sendo que na \autoref{fig:at1-am-dsb-sc}.(f) percebe-se
o sinal modulande centrado nas frequências da portadora. Além disso, ocorre a inversão de fase no sinal modulado da \autoref{fig:at1-am-dsb-sc}.(e).

Para a demodulação do sinal foi realizada a multiplicação do sinal modulado pela portadora, obtendo
como resultado a \autoref{fig:at1-am-dsb-sc}.(g). Entretanto esse processo faz 
com que seja possível obter a amplitude do sinal banda base a partir da onda portadora, 
contudo as frequências que compõe a onda portadora ainda permanecem presentes e deslocadas, conforme \autoref{fig:at1-am-dsb-sc}.(h).

Para remover a portadora e obter apenas o sinal banda base foi criado um filtro passa-baixa com frequência 
de corte de 1kHz. 

Após a passagem pelo filtro foi obtido o sinal demodulado da \autoref{fig:at1-am-dsb-sc}.(i), 
neste caso obteve-se o sinal banda base com sua amplitude reduzida pela metade, mas sem perder a informação. Seu espectro
de frequência contendo apenas a banda de 1kHz pode ser visto na \autoref{fig:at1-am-dsb-sc}.(j).

 
\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at1-am-dsb-sc.png}
    \caption{Simulação de modulação AM-DSB-SC. Fonte Própria.}
    \label{fig:at1-am-dsb-sc}
\end{figure}


\subsection{Modulação AM-DSB}

Para a modulação AM-DSB foi utilizada a portadora c(t) (\autoref{eq:c_t}) e o sinal modulante m(t) (\autoref{eq:m_t}). 
Além disso foram realizadas as modulações variando o fator de modulação com os seguinte valores: [0.25 0.5 0.75 1 1.5]

 
Na \autoref{fig:at1-am-dsb} é possível observar o sinal contido na envoltória superior de cada uma das modulações.
Contudo, conforme debatido na \autoref{sec:fator-am}, quanto mais o fator de modulação se aproxima de zero
maior é a potência transmitida na portadora. Esse efeito pode ser visto nos espectros da \autoref{fig:at1-am-dsb}.(b),
\autoref{fig:at1-am-dsb}.(d) e \autoref{fig:at1-am-dsb}.(f). 

Conforme o fator se aproxima de 1 menos potência existe na portadora,
entretanto a envoltória vai perdendo a característica da onda modulante e ocorre a inversão de fase no sinal modulado (\autoref{fig:at1-am-dsb}.(i)).


\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at1-am-dsb.png}
    \caption{Simulação de modulação AM-DSB e fator de modulação. Fonte Própria.}
    \label{fig:at1-am-dsb}
\end{figure}

\newpage
%------------------------------|
%       Atividade 2            |
%------------------------------|
\section{Atividade 2}

A primeira simulação consiste em realizar o processo de multiplexação por divisão de frequência de 3 sinais. 
A segunda simulação demostra o processo de recuperação do sinal multiplexado.

\subsection{Multiplexação FDM}
\label{sec:mux}

Para a realização da multiplexação foram simulados 3 sinais de frequências 1kHz, 2kHz e 3kHz, conforme a \autoref{eq:s1}, 
\autoref{eq:s2} e \autoref{eq:s3}, respectivamente. Os sinais e seus espectros são apresentados \autoref{fig:at2-sinais}

\begin{equation}
    s_1(t) = 2\times\cos (2\pi\times10^3t)
    \label{eq:s1}
 \end{equation}

 \begin{equation}
    s_2(t) = 2\times\cos (4\pi\times10^3t)
     \label{eq:s2}
 \end{equation}

 \begin{equation}
    s_3(t) = 2\times\cos (6\pi\times10^3t)
     \label{eq:s3}
 \end{equation}

 \begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at2-sinais.png}
    \caption{Sinais banda base. Fonte Própria.}
    \label{fig:at2-sinais}
\end{figure}

Para realizar a modulação dos sinais, a fim de deslocá-los para as frequências de multiplexação,
 foram criados três osciladores de frequências 10kHz, 12kHz e 14kHz, conforme a \autoref{eq:osc1}, 
 \autoref{eq:osc2} e \autoref{eq:osc3}, respectivamente. Os sinais que compões os osciladores e seus
 espectros estão na \autoref{fig:at2-osciladores}.

\begin{equation}
    osc_1(t) = \cos (20\pi\times10^3t)
    \label{eq:osc1}
 \end{equation}

 \begin{equation}
    osc_2(t) = \cos (24\pi\times10^3t)
     \label{eq:osc2}
 \end{equation}

 \begin{equation}
    osc_3(t) = \cos (28\pi\times10^3t)
     \label{eq:osc3}
 \end{equation}

 \begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at2-osciladores.png}
    \caption{Osciladores. Fonte Própria.}
    \label{fig:at2-osciladores}
\end{figure}

Além disso, para não haver sobreposição de frequências no espectro do sinal multiplexado foram criados três
filtros passa faixa, com frequências de corte de 10kHz-12kHz, 13kHz-15KHz e de 16kHz-18kHz. 

A \autoref{fig:at2-mux-esquema} contém o esquema do processo de multiplexação realizado nesta atividade.

\begin{figure}[ht]
    \centering
    \includegraphics[width=.6\linewidth]{figuras/at2-mux.png}
    \caption{Esquema de multiplexação. Fonte Própria.}
    \label{fig:at2-mux-esquema}
\end{figure}

Em que: 
\begin{itemize}
    \item s\textsubscript{1} é modulado por osc\textsubscript{1} (Espectro na \autoref{fig:at2-sinais-trans}.a);
    \item s\textsubscript{2} é modulado por osc\textsubscript{2} (Espectro na \autoref{fig:at2-sinais-trans}.b);
    \item s\textsubscript{3} é modulado por osc\textsubscript{3} (Espectro na \autoref{fig:at2-sinais-trans}.c);
    \item a saída de cada processo de modulação é direcionado para um filtro passa faixa (Espectros na \autoref{fig:at2-sinais-tr-filt} [a b e c]); ;
    \item os resultados das filtragens são multiplexados através da soma de todos os sinais (Espectro na \autoref{fig:at2-sinais-tr-filt}.d).
\end{itemize}


\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at2_sinais-transl.png}
    \caption{Sinais após modulação. Fonte Própria.}
    \label{fig:at2-sinais-trans}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at2-sinais-tr-filt.png}
    \caption{Sinais após a filtragem e multiplexação. Fonte Própria.}
    \label{fig:at2-sinais-tr-filt}
\end{figure}

\subsection{Demultiplexação FDM}
\label{sec:demux}

Para a realização da demultiplexação foram utilizados os osciladores apresentados na \autoref{eq:osc1}, 
\autoref{eq:osc2} e \autoref{eq:osc3} e os filtros passa faixa descritos na \autoref{sec:mux}.

A \autoref{fig:at2-demux-esquema} contém o esquema de demultiplexação do sinal em que:

\begin{itemize}
    \item o sinal multiplexado passa pelos três filtros(Espectros na \autoref{fig:at2-sig-mux-filt} [a b e c]);
    \item y\textsubscript{1} é demodulado pelo osc\textsubscript{1} (Espectro na \autoref{fig:at2-sinais-dem}.b);
    \item y\textsubscript{2} é demodulado pelo osc\textsubscript{2} (Espectro na \autoref{fig:at2-sinais-dem}.d);
    \item y\textsubscript{3} é demodulado pelo osc\textsubscript{3} (Espectro na \autoref{fig:at2-sinais-dem}.f);
    \item cada sinal passa por um filtro para remover as frequências espúrias (Espectro na \autoref{fig:at2-sinais-recuperados}).
\end{itemize}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.6\linewidth]{figuras/at2-demux.png}
    \caption{Esquema de demultiplexação. Fonte Própria.}
    \label{fig:at2-demux-esquema}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at2-sig-mux-filt.png}
    \caption{Sinal multiplexado após a passagem pelos filtros passa faixa. Fonte Própria.}
    \label{fig:at2-sig-mux-filt}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.7\linewidth]{figuras/at2-sinais-dem.png}
    \caption{Sinais demodulados. Fonte Própria.}
    \label{fig:at2-sinais-dem}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=.8\linewidth]{figuras/at2-sinais-recuperados.png}
    \caption{Sinais recuperados. Fonte Própria.}
    \label{fig:at2-sinais-recuperados}
\end{figure}


\newpage
\section{Conclusão}
\label{sec:conclusão}

Este trabalho apresentou os conceitos sobre Modulação em Amplitude e Multiplexação por Divisão de Frequência
abordados em sala de aula. Na \autoref{cap:introducao} foi apresentado o escopo geral do trabalho e 
seus principais objetivos. Na \autoref{sec:conceitos} foram esclarecidos brevemente os 
principais tópicos utilizados para o desenvolvimento do laboratório e nas \autoref{sec:mux} e \autoref{sec:demux}
foram descritas as atividades desenvolvidas e os resultados obtidos.


\section{Códigos desenvolvidos}
\label{sec:code_dev}

Segue o \href{https://github.com/sarom-torres/COM1/tree/master/AT-2020.2/Codigos_AE1}{link}
 para o acesso aos códigos.



% ----------------------------------------------------------
% Referências bibliográficas
% ----------------------------------------------------------
%\bibliography{referencias}
\end{document}


