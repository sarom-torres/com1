#------ATIVIDADE-2
#------AUTHOR
#SAROM TORRES

clc; clear all; close all;

%FREQUENCIAS
f = [1e3 2e3 3e3];
fo = [10e3 12e3 14e3];
N = 100;
fs = f(3)*N;

%TEMPOS
T = 1/f(1);
Ts = 1/fs; 
qt_periodo = 250;
t_final = qt_periodo*T;
t = [0:Ts:t_final];

passo_freq = 1/t_final;
freq = [-fs/2:passo_freq:fs/2];  

%-------------------------|
%       TRANSMISSOR       |
%-------------------------|
%SINAIS
A = 2;
s1 = A*cos(2*pi*f(1)*t);
s2 = A*cos(2*pi*f(2)*t);
s3 = A*cos(2*pi*f(3)*t);

%OSCILADORES
Ao = 1;
osc1 = Ao*cos(2*pi*fo(1)*t);
osc2 = Ao*cos(2*pi*fo(2)*t);
osc3 = Ao*cos(2*pi*fo(3)*t);

%SINAIS TRANSLADADOS
s1_transl = s1.*osc1;
s2_transl = s2.*osc2;
s3_transl = s3.*osc3;

%FILTROS
ord = 300;
wn1 = [(10.5e3*2)/fs (12e3*2)/fs];
wn2 = [(13e3*2)/fs (15e3*2)/fs];
wn3 = [(16e3*2)/fs (18e3*2)/fs];
filtro_PF1 = fir1(ord,wn1);
filtro_PF2 = fir1(ord,wn2);
filtro_PF3 = fir1(ord,wn3);


%SINAIS FILTRADOS
s1_tr_filtr = filter(filtro_PF1,1,s1_transl);
s2_tr_filtr = filter(filtro_PF2,1,s2_transl);
s3_tr_filtr = filter(filtro_PF3,1,s3_transl);


%SINAL MULTIPLEXADO
sig_mux = s1_tr_filtr + s2_tr_filtr + s3_tr_filtr;


%TRANSFORMADAS
S1 = fft(s1)/length(s1);
S2 = fft(s2)/length(s2);
S3 = fft(s3)/length(s3);

OSC1 = fft(osc1)/length(osc1);
OSC2 = fft(osc2)/length(osc2);
OSC3 = fft(osc3)/length(osc3);

S1_T = fft(s1_transl)/length(s1_transl);
S2_T = fft(s2_transl)/length(s2_transl);
S3_T = fft(s3_transl)/length(s3_transl);

S1_T_FLT = fft(s1_tr_filtr)/length(s1_tr_filtr);
S2_T_FLT = fft(s2_tr_filtr)/length(s2_tr_filtr);
S3_T_FLT = fft(s3_tr_filtr)/length(s3_tr_filtr);


SIG_MUX = fft(sig_mux)/length(sig_mux);


%-------------------------|
%         RECEPTOR        |
%-------------------------|

% SINAL MULTIPLEXADO FILTRADO
s1_rc_filt = filter(filtro_PF1,1,sig_mux);
s2_rc_filt = filter(filtro_PF2,1,sig_mux);
s3_rc_filt = filter(filtro_PF3,1,sig_mux);

%DEMODULAÇÃO
s1_dem = s1_rc_filt.*osc1;
s2_dem = s2_rc_filt.*osc2;
s3_dem = s3_rc_filt.*osc3;

%FILTROS
ord = 300;
wn1 = (2e3*2)/fs;
wn2 = [(1e3*2)/fs (3e3*2)/fs];
wn3 = [(2e3*2)/fs (4e3*2)/fs];
filtro_rec_PF1 = fir1(ord,wn1);
filtro_rec_PF2 = fir1(ord,wn2);
filtro_rec_PF3 = fir1(ord,wn3);

% SINAL DEMODULADO FILTRADO
s1_dem_filt = filter(filtro_rec_PF1,1,s1_dem);
s2_dem_filt = filter(filtro_rec_PF2,1,s2_dem);
s3_dem_filt = filter(filtro_rec_PF3,1,s3_dem);

%TRANSFORMADAS
S1_RC_FILT = fft(s1_rc_filt)/length(s1_rc_filt);
S2_RC_FILT = fft(s2_rc_filt)/length(s2_rc_filt);
S3_RC_FILT = fft(s3_rc_filt)/length(s3_rc_filt);

S1_DEM = fft(s1_dem)/length(s1_dem);
S2_DEM = fft(s2_dem)/length(s2_dem);
S3_DEM = fft(s3_dem)/length(s3_dem);

S1_DEM_FILT = fft(s1_dem_filt)/length(s1_dem_filt);
S2_DEM_FILT = fft(s2_dem_filt)/length(s2_dem_filt);
S3_DEM_FILT = fft(s3_dem_filt)/length(s3_dem_filt);

%--------------------------|
%       PLOT SINAIS        |
%--------------------------|
%TEMPO
figure(1)
subplot(321);
plot(1e3*t,s1,'m','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(a) - Sinal s1(t)');

subplot(323);
plot(1e3*t,s2,'b','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(c) - Sinal s2(t)');

subplot(325);
plot(1e3*t,s3,'g','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(e) - Sinal s3(t)');
%FREQUENCIA
subplot(322);
plot(freq/1e3,abs(fftshift(S1)),'-mo','LineWidth',2);
grid on
axis([(-f(1)/1e3-2) (f(1)/1e3+2) 0 2])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro de s1(t)');

subplot(324);
plot(freq/1e3,abs(fftshift(S2)),'-bo','LineWidth',2);
grid on
axis([(-f(2)/1e3-2) (f(2)/1e3+2) 0 2])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d) - Espectro de s2(t)');

subplot(326);
plot(freq/1e3,abs(fftshift(S3)),'-go','LineWidth',2);
grid on
axis([(-f(3)/1e3-2) (f(3)/1e3+2) 0 2])
xticks([-5 -3  0 3 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(g) - Espectro de s3(t)');
%--------------------------|
%     PLOT OSCILADORES     |
%--------------------------|
%----------TEMPO
figure(2)
subplot(321);
plot(1e3*t,osc1,'r','LineWidth',2);
axis([0 1 (-Ao-1) (Ao+1)]);
grid on
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(a) - Oscilador de 10kHz');

subplot(323);
plot(1e3*t,osc2,'m','LineWidth',2);
axis([0 1 (-Ao-1) (Ao+1)]);
grid on
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(c) - Oscilador de 12kHz');

subplot(325);
plot(1e3*t,osc3,'b','LineWidth',2);
axis([0 1 (-Ao-1) (Ao+1)]);
grid on
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(e) - Oscilador de 14kHz');
%-----------FREQUENCIA
subplot(322);
plot(freq/1e3,abs(fftshift(OSC1)),'-ro','LineWidth',2);
grid on
axis([(-fo(1)/1e3-2) (fo(1)/1e3+2) 0 1])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro do Oscilador de 10kHz');

subplot(324);
plot(freq/1e3,abs(fftshift(OSC2)),'-mo','LineWidth',2);
grid on
axis([(-fo(2)/1e3-2) (fo(2)/1e3+2) 0 1])
xticks([-12 -5 0 5 12])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d) - Espectro do Oscilador de 12kHz');

subplot(326);
plot(freq/1e3,abs(fftshift(OSC3)),'-bo','LineWidth',2);
grid on
axis([(-fo(3)/1e3-2) (fo(3)/1e3+2) 0 1])
xticks([-14 -10 -5 0 5 10 14])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(g) - Espectro do Oscilador de 14kHz');

%--------------------------|
% PLOT SINAIS TRANSLADADOS |
%--------------------------|
%FREQUENCIA
figure(3)
subplot(311);
plot(freq/1e3,abs(fftshift(S1_T)),'-bo','LineWidth',2);
grid on
axis([(-fo(1)/1e3-5) (fo(1)/1e3+5) 0 1])
xticks([-11 -10 -9 0 9 10 11])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(a) - Espectro de s1(t) transladado em 10kHz');

subplot(312);
plot(freq/1e3,abs(fftshift(S2_T)),'-ro','LineWidth',2);
grid on
axis([(-fo(2)/1e3-5) (fo(2)/1e3+5) 0 1])
xticks([-14 -12 -10 0 10 12 14])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro de s2(t) transladado em 12kHz');

subplot(313);
plot(freq/1e3,abs(fftshift(S3_T)),'-ko','LineWidth',2);
grid on
axis([(-fo(3)/1e3-5) (fo(3)/1e3+5) 0 1])
xticks([-17 -14 -11 0 11 14 17])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(c) - Espectro de s3(t) transladado em 14kHz');


%------------------------------|
% PLOT SINAIS TRANSL FILTRADOS |
%------------------------------|
%FREQUENCIA
figure(4)
subplot(411);
plot(freq/1e3,abs(fftshift(S1_T_FLT)),'-bo','LineWidth',2);
grid on
axis([(-fo(1)/1e3-3) (fo(1)/1e3+3) 0 0.8])
xticks([-11 -5 0 5 11])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(a) - Espectro de s1(t) transladado e filtrado');

subplot(412);
plot(freq/1e3,abs(fftshift(S2_T_FLT)),'-ro','LineWidth',2);
grid on
axis([(-fo(2)/1e3-5) (fo(2)/1e3+5) 0 0.8])
xticks([ -14 -10 -5 0 5 10 14])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro de s2(t) transladado e filtrado');

subplot(413);
plot(freq/1e3,abs(fftshift(S3_T_FLT)),'-mo','LineWidth',2);
grid on
axis([(-fo(3)/1e3-5) (fo(3)/1e3+5) 0 0.8])
xticks([-17 -15 -10 -5 0 5 10 15 17])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(c) - Espectro de s3(t) transladado e filtrado');

subplot(414)
plot(freq/1e3,abs(fftshift(SIG_MUX)),'-ko','LineWidth',2);
grid on
axis([-20 20 0 1])
xticks([-17 -14 -11 0 11 14 17])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d) - Espectros dos sinais s1(t), s2(t) e s3(t) multiplexados');


%--------------------------|
% PLOT SINAIS REC SIG_MUX  |
%--------------------------|
%FREQUENCIA
figure(5)
subplot(311);
plot(freq/1e3,abs(fftshift(S1_RC_FILT)),'-go','LineWidth',2);
grid on
axis([(-fo(1)/1e3-5) (fo(1)/1e3+5) 0 1])
xticks([-11 -10 -9 0 9 10 11])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(a) - Espectro de sig\_mux(t) filtrado');

subplot(312);
plot(freq/1e3,abs(fftshift(S2_RC_FILT)),'-mo','LineWidth',2);
grid on
axis([(-fo(2)/1e3-5) (fo(2)/1e3+5) 0 1])
xticks([-14 -12 -10 0 10 12 14])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro de sig\_mux(t) filtrado');

subplot(313);
plot(freq/1e3,abs(fftshift(S3_RC_FILT)),'-co','LineWidth',2);
grid on
axis([(-fo(3)/1e3-5) (fo(3)/1e3+5) 0 1])
xticks([-17 -14 -11 0 11 14 17])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(c) - Espectro de sig\_mux(t) filtrado');

%-----------------------------------|
%       PLOT SINAIS DEMODULADOS     |
%-----------------------------------|
%TEMPO
figure(6)
subplot(321);
plot(1e3*t,s1_dem,'r','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(a) - Sinal s1(t) demodulado');

subplot(323);
plot(1e3*t,s2_dem,'k','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(c) - Sinal s2(t) demodulado');

subplot(325);
plot(1e3*t,s3_dem,'b','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(e) - Sinal s3(t) demodulado');
%FREQUENCIA
subplot(322);
plot(freq/1e3,abs(fftshift(S1_DEM)),'-ro','LineWidth',2);
grid on
axis([(-f(1)/1e3-2) (f(1)/1e3+2) 0 0.5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro de s1(t) demodulado');

subplot(324);
plot(freq/1e3,abs(fftshift(S2_DEM)),'-ko','LineWidth',2);
grid on
axis([(-f(2)/1e3-2) (f(2)/1e3+2) 0 0.5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d) - Espectro de s2(t) demodulado');

subplot(326);
plot(freq/1e3,abs(fftshift(S3_DEM)),'-bo','LineWidth',2);
grid on
axis([(-f(3)/1e3-2) (f(3)/1e3+2) 0 0.5])
xticks([-5 -3  0 3 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(f) - Espectro de s3(t) demodulado');


%-----------------------------------|
%       PLOT SINAIS RECUPERADOS     |
%-----------------------------------|
%TEMPO
figure(7)
subplot(321);
plot(1e3*t,s1_dem_filt,'r','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(a) - Sinal s1(t) recuperado');

subplot(323);
plot(1e3*t,s2_dem_filt,'k','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(c) - Sinal s2(t) recuperado');

subplot(325);
plot(1e3*t,s3_dem_filt,'b','LineWidth',2);
axis([0 3 (-A-1) (A+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(e) - Sinal s3(t) recuperado');
%FREQUENCIA
subplot(322);
plot(freq/1e3,abs(fftshift(S1_DEM_FILT)),'-ro','LineWidth',2);
grid on
axis([(-f(1)/1e3-2) (f(1)/1e3+2) 0 0.5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro de s1(t) recuperado');

subplot(324);
plot(freq/1e3,abs(fftshift(S2_DEM_FILT)),'-ko','LineWidth',2);
grid on
axis([(-f(2)/1e3-2) (f(2)/1e3+2) 0 0.5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d) - Espectro de s2(t) recuperado');

subplot(326);
plot(freq/1e3,abs(fftshift(S3_DEM_FILT)),'-bo','LineWidth',2);
grid on
axis([(-f(3)/1e3-2) (f(3)/1e3+2) 0 0.5])
xticks([-5 -3  0 3 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(f) - Espectro de s3(t) recuperado');