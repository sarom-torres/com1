#------ATIVIDADE-1
#------AUTHOR
#SAROM TORRES

clc; clear all; close all;

%FREQUENCIAS
fm = 1e3; %frequencia do sinal modulande
fc = 10e3; % frequencia da portadora
N = 100; %numero de amostras
fs = fc*N; %frequencia de amostragem

%TEMPOS
T = 1/fm; %período do sinal
Ts = 1/fs; % período de amostragem
qt_periodo = 250; %quantidade de periodos
t_final = qt_periodo*T;
t = [0:Ts:t_final];

passo_freq = 1/t_final;
freq = [-fs/2:passo_freq:fs/2];  

%SINAIS
Am = 2; % amplitude sinal modulante
%Ao = 3; % componente dc
Ac = 1; % amplitude portadora

Ao = Am./[0.25 0.5 0.75 1 1.5];

m_t = Am*cos(2*pi*fm*t);
c_t = Ac*cos(2*pi*fc*t);

%MODULAÇÃO
mod_am_dsb = (Ao(1) + m_t).*c_t;
mod_am_dsb2 = (Ao(2) + m_t).*c_t;
mod_am_dsb3 = (Ao(3) + m_t).*c_t;
mod_am_dsb4 = (Ao(4) + m_t).*c_t;
mod_am_dsb5 = (Ao(5) + m_t).*c_t;

mod_am_dsb_sc = m_t.*c_t;

%DEMODULAÇÃO
ord = 100;
filtro = fir1(ord,2*fm/fs);
dem_am_dsb_sc = mod_am_dsb_sc.*c_t;

mt_dem_filtrado = filter(filtro,1,dem_am_dsb_sc);
#mt_dem_filtrado = conv(mod_am_dsb_sc,filtro_PB);
#mt_dem_filtrado = dem_am_dsb_sc(1:end);
 
%TRANSFORMADAS
M = fft(m_t)/length(m_t);
C = fft(c_t)/length(c_t);
DSB1 = fft(mod_am_dsb)/length(mod_am_dsb);
DSB2 = fft(mod_am_dsb2)/length(mod_am_dsb2);
DSB3 = fft(mod_am_dsb3)/length(mod_am_dsb3);
DSB4 = fft(mod_am_dsb4)/length(mod_am_dsb4);
DSB5 = fft(mod_am_dsb5)/length(mod_am_dsb5);

DSB_SC = fft(mod_am_dsb_sc)/length(mod_am_dsb_sc);
DEM_DSB_SC = fft(dem_am_dsb_sc)/length(dem_am_dsb_sc);
M_DEM_FILTRADO = fft(mt_dem_filtrado)/length(mt_dem_filtrado);


##%PLOT SENOS NO TEMPO
figure(1)
subplot(521);
plot(1e3*t,m_t,'m','LineWidth',2);
axis([0 3 (-Am-1) (Am+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(a) - Sinal modulante');

subplot(523);
plot(1e3*t,c_t,'g','LineWidth',2);
axis([0 3 (-Ac-1) (Ac+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)')
title('(c) - Onda portadora');

subplot(525);
plot(1e3*t,mod_am_dsb_sc,'b','LineWidth',2);
axis([0 3 -3 3]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(e) - Modulação AM-DSB-SC');

subplot(527);
plot(1e3*t,dem_am_dsb_sc,'r','LineWidth',2);
axis([0 3 -3 3]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(g)- Demodulação do sinal AM-DSB-SC');

subplot(529);
plot(1e3*t,mt_dem_filtrado,'k','LineWidth',2);
axis([0 3 -2 2]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(h)- Filtragem do sinal demodulado');

%PLOT SENOS NA FREQUÊNCIA
subplot(522);
plot(freq/1e3,abs(fftshift(M)),'-mo','LineWidth',2);
grid on
axis([(-fm/1e3-2) (fm/1e3+2) 0 2])
%xticks([-5 -3 -1 0 1 3 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro do sinal modulante');

subplot(524);
plot(freq/1e3,abs(fftshift(C)),'-go','LineWidth',2);
grid on
axis([(-fc/1e3-2) (fc/1e3+2) 0 1])
%xticks([-5 -3 -1 0 1 3 5])
yticks([0.1 0.3 0.5 0.7 0.9])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d) - Espectro da onda portadora');

subplot(526);
plot(freq/1e3,abs(fftshift(DSB_SC)),'-bo','LineWidth',2);
grid on
axis([(-fc/1e3-2) (fc/1e3+2) 0 1])
xticks([-11 -9 0 9 11])
yticks([0.1 0.3 0.5 0.7 0.9])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(f) - Espectro AM-DSB-SC');

subplot(528);
plot(freq/1e3,abs(fftshift(DEM_DSB_SC)),'-ro','LineWidth',2);
grid on
axis([-22 22 0 1])
xticks([-20 -10 -1 1 10 20])
yticks([0.1 0.3 0.5 0.7 0.9])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(h) - Espectro do sinal demodulado');

subplot(5,2,10);
plot(freq/1e3,abs(fftshift(M_DEM_FILTRADO)),'-ko','LineWidth',2);
grid on
axis([(-fm/1e3-5) (fm/1e3+5) 0 1])
xticks([-5 -3 -1 0 1 3 5])
yticks([0.1 0.3 0.5 0.7 0.9])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(j) - Espectro do sinal filtrado e demodulado');


% PLOT MODULAÇÃO AM_DSB
figure(2)

% TEMPO
subplot(521);
plot(1e3*t,mod_am_dsb,'b','LineWidth',2);
axis([0 3 -12 12]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(a)- Sinal AM-DSB com fator modulação 0.25');

subplot(523);
plot(1e3*t,mod_am_dsb2,'r','LineWidth',2);
axis([0 3 -8 8]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(c)- Sinal AM-DSB com fator modulação 0.5');

subplot(525);
plot(1e3*t,mod_am_dsb3,'c','LineWidth',2);
axis([0 3 -6 6]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(e)- Sinal AM-DSB com fator modulação 0.75');

subplot(527);
plot(1e3*t,mod_am_dsb4,'m','LineWidth',2);
axis([0 3 -6 6]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(g)- Sinal AM-DSB com fator modulação 1.00');

subplot(529);
plot(1e3*t,mod_am_dsb5,'g','LineWidth',2);
axis([0 3 -4 4]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(i)- Sinal AM-DSB com fator modulação 1.25');

% FREQUENCIAS

subplot(522);
plot(freq/1e3,abs(fftshift(DSB1)),'-bo','LineWidth',2);
grid on
axis([(-fc/1e3-3) (fc/1e3+3) 0 5])
xticks([-11 -10 -9 0 9 10 11])
yticks([0 0.5 1 2 3 4 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b) - Espectro fator modulação 0.25 ');

subplot(524);
plot(freq/1e3,abs(fftshift(DSB2)),'-ro','LineWidth',2);
grid on
axis([(-fc/1e3-3) (fc/1e3+3) 0 3])
xticks([-11 -10 -9 0 9 10 11])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d) - Espectro fator modulação 0.5');

subplot(526);
plot(freq/1e3,abs(fftshift(DSB3)),'-co','LineWidth',2);
grid on
axis([(-fc/1e3-3) (fc/1e3+3) 0 1.75])
xticks([-11 -10 -9 0 9 10 11])
yticks([0 0.25 0.5 0.75 1 1.25 1.5 1.75])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(f) - Espectro fator modulação 0.75');

subplot(528);
plot(freq/1e3,abs(fftshift(DSB4)),'-mo','LineWidth',2);
grid on
axis([(-fc/1e3-3) (fc/1e3+3) 0 2])
xticks([-11 -10 -9 0 9 10 11])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(h)- Espectro fator modulação 1.00');

subplot(5,2,10);
plot(freq/1e3,abs(fftshift(DSB5)),'-go','LineWidth',2);
grid on
axis([(-fc/1e3-3) (fc/1e3+3) 0 1])
xticks([-11 -10 -9 0 9 10 11])
yticks([0.1 0.3 0.5 0.6 0.7 0.9])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(h) - Espectro fator modulação 1.25');



