#------AUTHOR
#SAROM TORRES

clc;clear all;close all;

%FREQUENCIAS
f = 10e3; %frequencia do sinal
f1 = 20e3;
f2 = 5e3;
N = 100; %numero de amostras
fs = f*N; %frequencia de amostragem

%TEMPOS
T = 1/f; %período do sinal
Ts = 1/fs; % período de amostragem
qt_periodo = 250; %quantidade de periodos
t_final = qt_periodo*T;
t = [0:Ts:t_final];

passo_freq = 1/t_final;
freq = [-fs/2:passo_freq:fs/2];  

%COSSENOS
A0 = 5; 
A1 = 8;
A = 2;

y1 = A0*cos(f*t);
y2 = A1*cos(f1*t);


st = A+y1+y2;
 
%TRANSFORMADAS
Y1 = fft(y1)/length(y1);
Y2 = fft(y2)/length(y2);
Sf = fft(st)/length(st);

%POTENCIA
pot = (norm(st)^2)/length(st)

%DENSIDADE ESPECTRAL DE ENERGIA
[dep,win] = pwelch(st,[],[],[],fs);


%PLOT SINAIS NO DOMÍNIO DO TEMPO
figure(1)

subplot(311);
plot(1e3*t,y1,'m','LineWidth',2);
axis([0 3 (-A0-1) (A0+1)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(a)');

subplot(312);
plot(1e3*t,y2,'g','LineWidth',2);
axis([0 3 (-10) (10)]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)')
title('(b)');


subplot(313);
plot(1e3*t,st,'r','LineWidth',2);
axis([0 3 -10 17]);
xlabel('Tempo (ms)');
ylabel('Amplitude (V)');
title('(d)');


%PLOT SINAIS NO DOMÍNIOO DA FREQUÊNCIA
figure(2)
subplot(311);
plot(freq/1e3,abs(fftshift(Y1)),'-mo','LineWidth',2);
grid on
axis([(-f2/1e3-2) (f2/1e3+2) 0 4])
xticks([-5 -3 -1 0 1 3 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(a)');

subplot(312);
plot(freq/1e3,abs(fftshift(Y2)),'-go','LineWidth',2);
grid on
axis([(-f2/1e3-2) (f2/1e3+2) 0 4 ])
xticks([-5 -3 -1 0 1 3 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(b)');

subplot(313);
plot(freq/1e3,abs(fftshift(Sf)),'-ro','LineWidth',2);
grid on
axis([(-f2/1e3-2) (f2/1e3+2) 0 4])
xticks([-5 -3 -1 0 1 3 5])
xlabel('Frequência (kHz)');
ylabel('Amplitude (V)');
title('(d)');

%PLOT DA DENSIDADE ESPECTRAL
figure(3)
plot(win/1e3,5*log10(dep),'b','LineWidth',2)
xlim([0 20]);
xticks([-5 -3 -1 0 1 3 5])
xlabel('Frequência (kHz)');
ylabel('DEP (dB/Hz)');
title('Densidade Espectral de Potência de s(t)');




