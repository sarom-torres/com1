clear all;
close all;
clc

f_info1 = 1e3;
f_info2 = 1e3;

f_osc1 = 20e3;
f_osc2 = 23e3;

Filtro_PF1 = [zeros(1,490e3) ones(1,1e3) zeros(1,18001) ones(1,1e3) zeros(1,490e3)];  
Filtro_PF2 = [zeros(1,488.5e3) ones(1,1e3) zeros(1,21001) ones(1,1e3) zeros(1,488.5e3)];  

N = 100;
fs = N*f_osc1;
ts = 1/fs;
A1 = 2;
A2 = 1;
A_osc = 1;

t_final = 0.5;
t = [0:ts:t_final];

info1 =A1*cos(2*pi*f_info1*t);
info2 =A2*cos(2*pi*f_info2*t);

osc1 = A_osc*cos(2*pi*f_osc1*t);
osc2 = A_osc*cos(2*pi*f_osc2*t);

sinal_transl1 = info1.*osc1;
sinal_transl2 = info2.*osc2;
#sinal_mux = sinal_transl1 + sinal_transl2;

f = [-fs/2:2:fs/2];

Info1 = fft(info1)/length(f);
Info1 = fftshift(Info1);
Info2 = fft(info2)/length(f);
Info2 = fftshift(Info2);

Osc1 = fft(osc1)/length(f);
Osc1 = fftshift(Osc1);
Osc2 = fft(osc2)/length(f);
Osc2 = fftshift(Osc2);

Sinal_Transl1 = fft(sinal_transl1)/length(f);
Sinal_Transl1 = fftshift(Sinal_Transl1);
Sinal_Transl2 = fft(sinal_transl2)/length(f);
Sinal_Transl2 = fftshift(Sinal_Transl2);

Sinal_T1 = Sinal_Transl1.*Filtro_PF1;
Sinal_T2 = Sinal_Transl2.*Filtro_PF2;



#Sinal_Mux = fft(sinal_mux)/length(f);
#Sinal_Mux = fftshift(Sinal_Mux);
Sinal_Mux = Sinal_T1 + Sinal_T2;

figure(1)
subplot(411)
plot(f,abs(Info1))
xlim([-10e3 10e3])
subplot(412)
plot(f,abs(Info2))
xlim([-10e3 10e3])
subplot(413)
plot(f, abs(Osc1))
xlim([-55e3 55e3])
subplot(414)
plot(f, abs(Osc2))
xlim([-55e3 55e3])

figure(2)
subplot(311)
plot(f, abs(Sinal_Transl1))
xlim([-55e3 55e3])
subplot(312)
plot(f, abs(Sinal_Transl2))
xlim([-55e3 55e3])
subplot(313)
plot(f,abs(Sinal_Mux))
xlim([-55e3 55e3])