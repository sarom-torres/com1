clear all
close all
clc

f1 = 1e3;
f2 = 2e3;
f_cut = 1.5e3;

N =20;
fs = N*f2;
Ts = 1/fs;

t = [0:Ts:1];

y = cos(2*pi*f1*t) + cos(2*pi*f2*t);

filtro_PF = fir1(100,(2*[10e3 12e3])/fs);
filtro_PB = fir1(100,(2*f_cut)/fs);
y_filter = filter(filtro_PB, 1, y);

figure(1)
subplot(211)
plot(t,y)
xlim([0 20/f1])
subplot(212)
plot(t,y_filter)
xlim([0 20/f1])

figure(2)
freqz(filtro_PB)

figure(3)
freqz(filtro_PF)