clear all
close all
clc

aux_analog = 100e4;
fs = 10e3;
fm = 1e3;

t = [0:1/aux_analog:1];

y = cos(2*pi*fm*t);

y_amostrado = y(1:aux_analog/fs:end);
k = t(1:aux_analog/fs:end);

subplot(211)
plot(t,y)
xlim([0 2/fm])
subplot(212)
stem(k,y_amostrado)
xlim([0 4/fm])